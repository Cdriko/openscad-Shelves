/*
rayonage
produit un rayonage parametrable constructible avec une decoupe laser

credits : Cédric Doutriaux 2015

licence : CC-BY-SA
*/

/////////////////////////////////////////parameters
flatten=1; //for representation(0) or export(1)




nRows=4;
nCols=3;
width=680;
height=290;
prof=310;
background=1;//planche AR ou pas

tabsL=100;
MatTrikness=5;
jeux=0.1;
/////////////////////////////////////////end parameters

rayonnages();


module rayonnages(){

///////////bords

//bas
if(flatten){
    color([0,1,0])
    rotate([0,0,-90])
bord(L=width,Step=nCols);
}else{
    bord(L=width,Step=nCols);
    }
    
    //haut
    if(flatten){
        color([0,1,1])
    rotate([0,0,-90])
        translate([0,prof+(MatTrikness*2),0])
bord(L=width,Step=nCols);
}else{
    translate([width,0,height-MatTrikness])
    rotate([0,-180,0])
    bord(L=width,Step=nCols);
    }

    //gauche
    if(flatten){
         color([1,1,0])
    rotate([0,0,-90])
        translate([0,(2*(prof+MatTrikness))+(MatTrikness*2),0])
bord(L=width,Step=nCols);
}else{
    translate([MatTrikness,0,height])
    rotate([0,90,0])
    bord(L=height,Step=nRows);
    }

    //droite
    if(flatten){
    rotate([0,0,-90])
        translate([0,(3*(prof+(MatTrikness*2))),0])
bord(L=width,Step=nCols);
}else{
    translate([width-MatTrikness,0,0])
    rotate([0,-90,0])
    bord(L=height,Step=nRows);
    }
    
    
//rayonnages
//verticales
 for(j= [1 : nCols-1]){
     rotate([0,0,90])
      if (flatten){
           translate([0,j*(prof+(MatTrikness*2)),0]) 
           //horizontale();
           planche(L=height,Step=nRows,back=1);
           } else {
                    rotate([90,-90,0])
               translate([0,-prof,j*(width/nCols)]) 
               planche(L=height,Step=nRows,back=1);
               }    
 }

///horizontales
   for(j= [1 : nRows-1]){

       if (flatten){
           translate([0,j*(prof+(MatTrikness*2)),0]) 
           //horizontale();
           planche(L=width,Step=nCols);
           } else {
               translate([0,0,j*(height/nRows)]) 
               planche(L=width,Step=nCols);
               }
   }

   if(background){
       
       if(flatten){
           color([1,0,0])
           translate([0,MatTrikness,0])
           arriere();
       }else{
       translate([0,prof,0])
       rotate([90,0,0])
       arriere();
       }
       
       
       }
   
   
   }
   
   
   
////////////////////////////////////////////////////////////////////////////////////////////MODULES
   
module arriere(){
    //planche arriere
    difference(){
    square([width,height]);
        
//nRows=3;
//nCols=3;
        
        union(){
            
            for(j = [0 : nRows]){
            for(i = [0 : nCols-1]){
             translate([(i*(width/nCols)+((width/nCols)/3)),(j*(((height-MatTrikness)/nRows)))])
             square([((width/nCols)/3),MatTrikness]);
            }
        }
        
         for(j = [0 : nCols]){
            for(i = [0 : nRows-1]){
              translate([(j*((width-MatTrikness)/nCols)),(i*(height/nRows)+((height/nRows)/3))])
             square([MatTrikness,((height/nRows)/3)]);
            }
        }
        
        
        
            
        }
        
        
        
    }
    
    
    
    
    }
   
   
   
   //planche extérieure
  module bord(L=width,Step=nCols){


     
      
      
    //lateral tabs
    //start
    translate([0,((prof-tabsL)/2)+jeux,0])
    square([MatTrikness,(tabsL-(2*jeux))]);
    
    //end

    translate([(L-MatTrikness),0,0]){
 encocheFemelle();
    }
    
    //calculated dimension of each case
    ofset=(L-((1+Step)*MatTrikness))/Step;
    //planches et encoches des colones
    for(i = [0 : Step-1]){
        
        
        translate([MatTrikness+i*(MatTrikness+ofset),0,0]){
        square([ofset,prof]);
            if(i<Step-1)
            translate([ofset,0,0])
             encocheFemelle();
          
        }
        
         if (background){
          //encoches arrières
                   translate([(i*(L/Step)+((L/Step)/3)),prof,0])
             square([((L/Step)/3),MatTrikness]);
          }
        
        
        }
 
    
    
} 

module encocheFemelle(){
       square([MatTrikness,((prof-tabsL)/2)-jeux]);
        translate([0,((prof-tabsL)/2)+jeux+tabsL,0])
        square([MatTrikness,((prof-tabsL)/2)-jeux]);
    
    
    }
   
   
   
//planche intermérdiaire
module planche(L=width,Step=nCols,back=0){


    //lateral tabs
    //left
    translate([0,((prof-tabsL)/2)+jeux,0])
    square([MatTrikness,(tabsL-(2*jeux))]);
    
    //right

    translate([(L-MatTrikness),((prof-tabsL)/2)+jeux,0])
    square([MatTrikness,(tabsL-(2*jeux))]);
    
    //calculated dimension of each case
    ofset=(L-((1+Step)*MatTrikness))/Step;
    //planches et encoches des colones
    for(i = [0 : Step-1]){
        
        
        translate([MatTrikness+i*(MatTrikness+ofset),0,0]){
        square([ofset,prof]);
            if(i<Step-1)
            translate([ofset,0,0])
            square([MatTrikness+2*jeux,prof/2]);
        }
        
                if (background){
          //encoches arrières
                  //
                    if(back){
                        translate([(i*(L/Step)+((L/Step)/3)),-MatTrikness,0])
                        square([((L/Step)/3),MatTrikness]);
                    }else{
                         translate([(i*(L/Step)+((L/Step)/3)),prof,0])
                        square([((L/Step)/3),MatTrikness]);
                        }
          }
        
        
        }
 
    
    
}


module plancheHorizontale(){
    //lateral tabs
    //left
    translate([0,((prof-tabsL)/2)+jeux,0])
    square([MatTrikness,(tabsL-(2*jeux))]);
    
    //right

    translate([(width-MatTrikness),((prof-tabsL)/2)+jeux,0])
    square([MatTrikness,(tabsL-(2*jeux))]);
    

    //planches et encoches des colones
    for(i = [0 : nCols-1]){
        
        
        translate([MatTrikness+i*(MatTrikness+ofsetCol),0,0]){
        square([ofsetCol,prof]);
            if(i<nCols-1)
            translate([ofsetCol,0,0])
            square([MatTrikness+2*jeux,prof/2]);
        }
        
        }
    
    
}


