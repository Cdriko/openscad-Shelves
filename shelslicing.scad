/*
shelslicing
slicing shelving
Generate a sliceform shelving from a 3D object

Rayonnage tranché
Génerer un rayonnage en sliceform d'après un fichier stl

credits :
Cédric Doutriaux
Rather kludgy for BoundingBox function

license : cc-by-sa
*/

use <text_on.scad> //for ids
//////////////////////////////////////////parameters
//representation
flatten=0; //--------for representation(0) assembly(-1) or export(1) and numbers(2)
Hpadding=100;//---padding for Horizontals
Vpadding=90;//----padding for Verticals
assemblyOfset=130;//ofset of parts for assembly view

//material
MatThickness=5;//material thikness/épaisseur du matériaux
clearance=0.1;//jeux fonctionnel


//structure
stlFile="exemple.stl";//la forme part de 0 et s'étent sur XY et Z positifs
PlanchesHorizontales=[50,100,190,280,350,400,450,550];//list of Horizontals with positions Z
PlanchesVerticales=[80,120,220,350,470,540,650];//list of Verticals with positions X


///////////////////////////////////////end parameters


//traitCoupe([50,0,60]);
//encoches();





rayonages();
//master();


module master(){
    //the master form to be sliced
    //need to be in X and Y and Z >0
    //flat along the XZ plane
    rotate([0,0,90])
    translate([0,-730,0])
scale([100,100,100])
import(stlFile, convexity=3);
    
}


module rayonages(H=PlanchesHorizontales,V=PlanchesVerticales){
    //dessine des rayonnages avec les planches H et V        
    planchesH(H,flat=flatten);
    
    if (flatten==-1){//assembly view
        echo("rendering assembly view");
        
    translate([0,-assemblyOfset,0])
    planchesV(V); 
    }else{
        planchesV(V); 
        }
    
}



module encochesAV(H=PlanchesHorizontales,V=PlanchesVerticales){
    ///matrice de toutes les encoches
    render(){
    union(){
        for(Z = H){
                for(X=V){
                    color([X/1000,0,1-(Z/1000)])
                    
                    echo(str("traitCoupe([",X,",0,",Z,"]); ")); 
                    difference(){
                        
                    traitCoupe([X,0,Z]);   
                       scale([1,0.5,1]) 
                        traitCoupe([X,0,Z]);    
                    }                
                }    
         }
     }
 }
 }



module encochesAR(H=PlanchesHorizontales,V=PlanchesVerticales){
    ///matrice de toutes les encoches
    render(){
    union(){
        for(Z = H){
                for(X=V){
                    traitCoupe([X,0,Z]);                    
                }    
         }
     }
 }
 }
    
module planchesH(H=PlanchesHorizontales,flat=0){
//planches horizontales

    render(){
        
    for(i=[0:1:len(H)-1]){//pour chaque planche horizontale
        position=H[i];
        echo(i,position);
        if (flat==1) {///flatten
            translate([0,Hpadding*i,0])
            projection(cut = false)
            plancheH(position);
            }else if (flat==2){//numbers only
                translate([50,(Hpadding*i)+30,0])
            projection(cut = false)
                text_extrude(str("h",i),size=16,extrusion_height=2,center=true);
                }else{//3D
                     plancheH(position);
                    }
        
    }
}

module plancheH(position){
  
    //color([position/120,1-(position/120),0])
                difference(){           
                       
                            intersection(){//planche sans encoches a la forme du moule
                            translate([0,0,position])
                            cube([2000,2000,MatThickness]);
                            master();
                           
                    }        
                    scale([1,0.5,1])//half cut
                    encochesAR();
                }
        }
    
    
    
    }


module planchesV(V=PlanchesVerticales){
//planches verticales
    
   for(j=[0:1:len(V)-1]){//pour chaque planche verticale
       position=V[j];
       if (flatten==1){          
           
           projection(cut = false) 
           rotate([0,-90,0])
          
           translate([0,Vpadding*j,5])
           plancheV(position);
           }else if (flatten==2){//numbers only
                translate([-150,(Vpadding*j)+30,0])
            projection(cut = false)
                text_extrude(str("V",j),size=16,extrusion_height=2,center=true);
       }else{
           plancheV(position);
       }
            
 
}
}


module plancheV(position){
 
    color([position/max(PlanchesVerticales),1-(position/(max(PlanchesVerticales)-20)),0.1])
            difference(){           
                   
                        intersection(){//planche sans encoches a la forme du moule
                        translate([position,0,0])
                        cube([MatThickness,1000,1000,]);
                        master();
                       
                }        
                
                encochesAV();
            }
   
    }


module traitCoupe(position=[0,0,0]){
//trait de coupe
    BoundingBox() 
    intersection(){
            master();
            translate(position)
            cube([MatThickness+(2*clearance),1000,MatThickness+(2*clearance)]);
        
        
        }
    //}
}



// Rather kludgy module for determining bounding box from intersecting projections
module BoundingBox()
{
        intersection()
        {
                translate([0,0,0])
                linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                projection(cut=false) intersection()
                {
                        rotate([0,90,0]) 
                        linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                        projection(cut=false) 
                        rotate([0,-90,0]) 
                        children(0);

                        rotate([90,0,0]) 
                        linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                        projection(cut=false) 
                        rotate([-90,0,0]) 
                        children(0);
                }
                rotate([90,0,0]) 
                linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                projection(cut=false) 
                rotate([-90,0,0])
                intersection()
                {
                        rotate([0,90,0]) 
                        linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                        projection(cut=false) 
                        rotate([0,-90,0]) 
                        children(0);

                        rotate([0,0,0]) 
                        linear_extrude(height = 10000, center = true, convexity = 10, twist = 0) 
                        projection(cut=false) 
                        rotate([0,0,0]) 
                        children(0);
                }
        }
}

